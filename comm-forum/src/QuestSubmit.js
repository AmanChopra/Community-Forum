import React from 'react'
import './QuestSubmit.css'

const QuestSubmit = () => {
    return (
        <div className='main__box'>
            <div className='submit__heading'>
                <h2>Question sent to the Doctor's Successfully</h2>
            </div>

            <div className='submit__stat'>
                <p>
                    Please wait for a few hours to get a response from our doctors.  <br />
                    In the meanwhile if you think it is urgent, please book a consultation to talk to a doctor virtually. 
                </p>
            </div>

            <div className="d-grid gap-2 col-3 mx-auto my-3">
                <button type="button" class="btn btn-dark">Go to all Question</button>
                <button type="button" class="btn btn-dark">Book consultation</button>
            </div>

        </div>
    )
}

export default QuestSubmit